<?php

namespace ProjetAnimalerie;

require "vendor/autoload.php";

use ProjetAnimalerie\Model\Classes\Chat;
use ProjetAnimalerie\Model\Classes\Chien;
use ProjetAnimalerie\Controller\ChatController;

$page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);
$debugMode = true;

if (!isset($page))
{
    $page = "accueil";
}

switch ($page)
{
    case "accueil":
        include "View/templates/accueil.html";
        break;
    case "chats":
        ChatController::listAction();
        break;
    case "chat":
        $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        ChatController::readAction($id);
        break;
    case "chat-add":
        ChatController::addAction();
        break;
    case "chat-add-submit":
        ChatController::addSubmitAction();
        break;
    case "chat-edit":
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        ChatController::editAction($id);
        break;
    case "chat-edit-submit":
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        ChatController::editSubmitAction($id);
        break;
    case "chat-remove":
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        ChatController::removeAction($id);
        break;
    case "chat-remove-submit":
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        ChatController::removeSubmitAction($id);
        break;
    case "chiens":
        $type = "chien";
        $type_pluriel = "chiens";
        $rows = Chien::getAll();
        include "src/View/templates/list_animal.html";
        break;
    case "chien":
        $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        $chien = Chien::get($id);
        include "View/templates/fiche_chien.html";
        break;
    default:
        $message = "Erreur 404 : Page inconnue&nbsp;!";
        include "View/templates/erreur.html";
        break;
}
