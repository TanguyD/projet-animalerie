<?php

namespace ProjetAnimalerie\Model\Dao;

use ProjetAnimalerie\Model\Dal\Dal;
use \PDO;

class ChatDao extends Dal
{
    private $classname = "ProjetAnimalerie\\Model\\Classes\\Chat";
    private $table = "chat";
    private $structure = ['id', 'nom', 'sexe', 'naissance', 'image','id_animal', 'pelage'];

    public function getAll()
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  INNER JOIN `animal` ON `chat`.`id_animal` = `animal`.`id`;
                  ";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }

    public function get($id)
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  INNER JOIN `animal` ON `chat`.`id_animal` = `animal`.`id`
                  WHERE `id` = :id;
                  ";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $object = $stmt->fetch();
        $this->Close();
        return $object;
    }

    public function insert($nom, $sexe, $naissance, $image, $pelage)
    {
        global $debugMode;
        $nbRows = 0;

        // Délégation à la classe AnimalDao
        $animalDao = new AnimalDao();
        $id_animal = $animalDao->insert($nom, $sexe, $naissance, $image);
        if ($id_animal)
        {
            $query = "INSERT INTO `{$this->table}`
            (`id_animal`, `pelage`)
            VALUES
            (:id_animal, :pelage);
            ";

            $this->Open();
            $stmt = $this->dbh->prepare($query);
            
            $stmt->bindParam(":id_animal", $id_animal, PDO::PARAM_INT);
            $stmt->bindParam(":pelage", $pelage, PDO::PARAM_STR);

            $nbRows = $stmt->execute();

            if ($debugMode && $nbRows != 1)
            {
                echo '<div class="alert alert-danger" role="alert">' . "\n";
                echo $stmt->errorInfo()[2];
                echo '</div>' . "\n";
            }

            $this->Close();
        }
        return $nbRows;
    }

    /**
     * Mise à jour d'un animal
     * @param int $id identifiant
     * @param string $nom nom
     * @param string $sexe Mâle ou Femelle
     * @param string $naissance YYYY-MM-DD
     * @param string $image nom du fichier image
     */
    public function update ($id, $nom, $sexe, $naissance, $image, $pelage)
    {
        global $debugMode;

        // Délégation à la classe AnimalDao
        $animalDao = new AnimalDao();
        $nbRows = $animalDao->update($id, $nom, $sexe, $naissance, $image);

        if ($nbRows)
        {
            $query = "UPDATE `{$this->table}`
                        SET
                            `pelage` = :pelage
                        WHERE `id_animal` = :id; 
                        ";

            $this->Open();
            $stmt = $this->dbh->prepare($query);

            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->bindParam(":pelage", $pelage, PDO::PARAM_STR);

            $nbRows = $stmt->execute();
            
            if ($debugMode && $nbRows != 1)
            {
                echo '<div class="alert alert-danger" role="alert">' . "\n";
                echo $stmt->errorInfo()[2];
                echo '</div>' . "\n";
            }

            $this->Close();
        }

        return $nbRows;
    }

    /**
     * Suppression d'un animal
     * @param int $id identifiant de l'animal
     */
    public function delete ($id)
    {
        $animalDao = new AnimalDao();
        $nbRows = $animalDao->delete($id);
        return $nbRows;
    }
}