<?php
namespace ProjetAnimalerie\Model\Dao;

use \DateTime;

class Tools
{
    /**
     * Convert MySql Date to French Format
     * @param string $date date to convert
     */
    public static function MySqlDateToFr($date)
    {
        $dt = DateTime::createFromFormat("Y-m-d", $date);
        return $dt->format("d/m/Y");
    }

    /**
     * Convert French Date to MySql Format
     * @param string $date date to convert
     */
    public static function FrDateToMySql($date)
    {
        var_dump($date);
        $dt = DateTime::createFromFormat("d/m/Y", $date);
        var_dump($dt);
        return $dt->format("Y-m-d");
    }
}