<?php
namespace ProjetAnimalerie\Model\Classes;

use \InvalidArgumentException;
use ProjetAnimalerie\Model\Dao\ChatDao;

class Chat extends Animal
{
    private $id_animal;
    private $pelage;

    protected function get_id_animal() {return $this->id_animal; }
    protected function set_id_animal($value) {$this->id_animal = $value; }

    protected function get_pelage() {return $this->pelage; }
    protected function set_pelage($value) {$this->pelage = $value; }

    public function __construct()
    {
        $nb = func_num_args();
        switch ($nb)
        {
            case 0:
                $this->construct_0();
                break;
            case 7:
                $id = func_get_arg(0);
                $nom = func_get_arg(1);
                $sexe = func_get_arg(2);
                $naissance = func_get_arg(3);
                $image = func_get_arg(4);
                $id_animal = func_get_arg(5);
                $pelage = func_get_arg(6);
                $this->construct_7($id, $nom, $sexe, $naissance, $image, $id_animal, $pelage);
                break;
            default:
                throw new InvalidArgumentException("Invalid parameters number");
                break;
        }
    }

    private function construct_0()
    {
        // valeurs par défaut

        // Constructeur de Animal
        parent::__construct();

        // Propriétés de Chat
        $this->id_animal = null;
        $this->pelage = "";
    }

    private function construct_7($id, $nom, $sexe, $naissance, $image, $id_animal, $pelage)
    {
        // Constructeur de Animal
        parent::__construct($id, $nom, $sexe, $naissance, $image);

        // valeurs fournies en paramètres
        $this->id_animal = $id;
        $this->pelage = $pelage;
    }

    public function __toString()
    {
        return $this->nom;
    }

    public static function getAll()
    {
        $dao = new ChatDao();
        return $dao->getAll();
    }

    public static function get($id)
    {
        $dao = new ChatDao();
        return $dao->get($id);
    }

    public static function insert($nom, $sexe, $naissance, $image, $pelage)
    {
        $dao = new ChatDao();
        return $dao->insert($nom, $sexe, $naissance, $image, $pelage);
    }

    /**
     * Mise à jour d'un animal
     * @param int $id identifiant
     * @param string $nom nom
     * @param string $sexe Mâle ou Femelle
     * @param string $naissance YYYY-MM-DD
     * @param string $image nom du fichier image
     */
    public static function update ($id, $nom, $sexe, $naissance, $image, $pelage)
    {
        $dao = new ChatDao();
        return $dao->update($id, $nom, $sexe, $naissance, $image, $pelage);
    }

    /**
     * Suppression d'un animal
     * @param int $id identifiant de l'animal
     */
    public static function delete ($id)
    {
        $dao = new ChatDao();
        return $dao->delete($id);
    }
}