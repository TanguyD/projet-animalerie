<?php
namespace ProjetAnimalerie\Model\Classes;

use \InvalidArgumentException;
use \DateTime;

class Animal extends Objet
{
    protected $id;
    protected $nom;
    protected $sexe;
    protected $naissance;
    protected $image;

    protected function get_id() {return $this->id; }
    protected function set_id($value) {$this->id = $value; }

    protected function get_nom() {return $this->nom; }
    protected function set_nom($value) {$this->nom = $value; }

    protected function get_sexe() {return $this->sexe; }
    protected function set_sexe($value) {$this->sexe = $value; }

    protected function get_naissance() {return $this->naissance; }
    protected function set_naissance($value) {$this->naissance = $value; }

    protected function get_image() {return $this->image; }
    protected function set_image($value) {$this->image = $value; }

    public function __construct()
    {
        $nb = func_num_args();
        switch ($nb)
        {
            case 0:
                $this->construct_0();
                break;
            case 5:
                $id = func_get_arg(0);
                $nom = func_get_arg(1);
                $sexe = func_get_arg(2);
                $naissance = func_get_arg(3);
                $image = func_get_arg(4);
                $this->construct_5($id, $nom, $sexe, $naissance, $image);
                break;
            default:
                throw new InvalidArgumentException("Invalid parameters number");
                break;
        }
    }

    private function construct_0()
    {
        // valeurs par défaut
        $this->id = null;
        $this->nom = "";
        $this->sexe = "";
        $this->naissance = null;
        $this->image = "";
    }

    private function construct_5($id, $nom, $sexe, $naissance, $image)
    {
        // valeurs fournies en paramètres
        $this->id = $id;
        $this->nom = $nom;
        $this->sexe = $sexe;
        $this->naissance = $naissance;
        $this->image = $image;
    }

    public function __toString()
    {
        return $this->nom;
    }

    public function age()
    {
        $dt_naissance = new DateTime($this->naissance);
        $dt_aujourdhui = new DateTime();
        $interval = $dt_aujourdhui->diff($dt_naissance);
        $y = $interval->format('%y');
        switch ($y)
        {
            case 0:
                $m = $interval->format('%m');
                $resultat = $m . " mois";
                break;
            case 1:
                $resultat = $y . " an";
                break;
            default:
                $resultat = $y . " ans";
                break;
        }
        return $resultat;
    }
}