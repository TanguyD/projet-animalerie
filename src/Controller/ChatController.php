<?php

namespace ProjetAnimalerie\Controller;

use ProjetAnimalerie\Model\Classes\Chat;
use ProjetAnimalerie\Model\Dao\AnimalDao;
use \Exception;

class ChatController
{
    /**
     * Affichage de la liste des chats
     * @author David RIEHL <david.riehl@ac-lille.fr>
     * @version 1.0
     */
    public static function listAction()
    {
        $type = "chat";
        $type_pluriel = "chats";
        $rows = Chat::getAll();
        include "src/View/templates/list_animal.html";
    }

    /**
     * Affiche un chat d'après son id
     * @param int id identifiant du chat
     */
    public static function readAction($id)
    {
        $chat = Chat::get($id);
        include "src/View/templates/fiche_chat.html";
    }

    /**
     * Affiche le formulaire d'ajout d'un nouveau chat
     */
    public static function addAction()
    {
        $type = "chat";
        $action = "add";
        include "src/View/templates/add_edit_animal.html";
    }

    /**
     * Traitement du formulaire d'ajout d'un nouveau chat
     */
    public static function addSubmitAction()
    {
        global $debugMode;

        $nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING);

        $regSexe = ["options" => ["regexp" => "/Mâle|Femelle/"]];
        $sexe = filter_input(INPUT_POST, 'sexe', FILTER_VALIDATE_REGEXP, $regSexe);
        
        $regNaissance = ["options" => ["regexp" => "/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/"]];
        $naissance = filter_input(INPUT_POST, 'naissance', FILTER_VALIDATE_REGEXP, $regNaissance);
        
        $pelage = filter_input(INPUT_POST, 'pelage', FILTER_SANITIZE_STRING);

        $result = ControllerTools::upload_file('image');

        if ($result['succeed'])
        {
            $image = $result['filename'];

            // sauvegarde dans la base de données
            $nbRows = Chat::insert($nom, $sexe, $naissance, $image, $pelage);

            if (! $debugMode || $nbRows)
            {
                // redirection vers la liste
                $host  = $_SERVER['HTTP_HOST'];
                $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                $extra = '?page=chats';
                header("Location: http://$host$uri/$extra");
            }
        }
    }

    /**
     * Affiche le formulaire d'édition d'un chat d'après son id
     * @param int $id identifiant du chat
     */
    public static function editAction($id)
    {
        $type = "chat";
        $action = "edit";
        $animal = Chat::Get($id);
        include "src/View/templates/add_edit_animal.html";
    }

    /**
     * Traitement du formulaire d'édition d'un chat
     * @param int $id identifiant du chat
     */
    public static function editSubmitAction($id)
    {
        global $debugMode;

        $nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING);

        $regSexe = ["options" => ["regexp" => "/Mâle|Femelle/"]];
        $sexe = filter_input(INPUT_POST, 'sexe', FILTER_VALIDATE_REGEXP, $regSexe);
        
        $regNaissance = ["options" => ["regexp" => "/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/"]];
        $naissance = filter_input(INPUT_POST, 'naissance', FILTER_VALIDATE_REGEXP, $regNaissance);
        
        $pelage = filter_input(INPUT_POST, 'pelage', FILTER_SANITIZE_STRING);

        $result = ControllerTools::upload_file('image');

        if ($result['succeed'])
        {
            $image = $result['filename'];

            // récupération des informations du chat
            $animal = Chat::get($id);
            $old_img = $animal->image;
            $ancienne_image = "src/View/images/" . $animal->image;
            
            // sauvegarde dans la base de données
            $nbRows = Chat::update($id, $nom, $sexe, $naissance, $image, $pelage);

            if ($nbRows && isset($image) && isset($old_img))
            {
                // le chat est mis à jour, on supprime l'ancienne image
                unlink($ancienne_image);
            }

            if (! $debugMode || $nbRows)
            {
                // redirection vers la liste
                $host  = $_SERVER['HTTP_HOST'];
                $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                $extra = '?page=chats';
                header("Location: http://$host$uri/$extra");
            }
        }
    }

    /**
     * Affiche le formulaire de suppression d'un chat d'après son id
     * @param int $id identifiant du chat
     */
    public static function removeAction($id)
    {
        $type = "chat";
        $animal = Chat::Get($id);
        include "src/View/templates/remove_animal.html";
    }

    /**
     * Traitement du formulaire de suppression d'un chat
     * @param int $id identifiant du chat
     */
    public static function removeSubmitAction($id)
    {
        global $debugMode;

        $nbRows = 1;

        if (isset($_POST['btn_yes']))
        {
            // on a cliqué sur [Oui]
            // récupérer les informations du chat à supprimer
            $animal = Chat::get($id);
            $filename = "src/View/images/" . $animal->image;
            // suppression dans la base de données
            $nbRows = Chat::delete($id);
            if ($nbRows)
            {
                // le chat est supprimé, on supprime l'image
                unlink($filename);
            }
        }

        if (! $debugMode || $nbRows)
        {
            // redirection vers la liste
            $host  = $_SERVER['HTTP_HOST'];
            $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
            $extra = '?page=chats';
            header("Location: http://$host$uri/$extra");
        }
    }
}