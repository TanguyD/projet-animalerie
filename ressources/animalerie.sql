-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 28 jan. 2019 à 12:17
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `animalerie`
--

-- --------------------------------------------------------

--
-- Structure de la table `animal`
--

DROP TABLE IF EXISTS `animal`;
CREATE TABLE IF NOT EXISTS `animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `sexe` enum('Mâle','Femelle') NOT NULL,
  `naissance` date NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `animal`
--

INSERT INTO `animal` (`id`, `nom`, `sexe`, `naissance`, `image`) VALUES
(1, 'Félix', 'Mâle', '2017-10-26', 'img_f3a94dd_20190118_121229.jpg'),
(2, 'Félindra', 'Femelle', '2018-02-10', 'chat2.jpg'),
(3, 'Garfield', 'Mâle', '2018-01-17', 'chat3.jpg'),
(4, 'Chéchur', 'Mâle', '2018-06-15', 'chat4.jpg'),
(5, 'Rominet', 'Mâle', '2014-06-06', 'chat5.jpg'),
(6, 'Médor', 'Mâle', '2017-07-08', 'chien6.jpg'),
(7, 'Bob', 'Mâle', '2017-01-01', 'chien7.jpg'),
(8, 'Ficelle', 'Femelle', '2018-03-21', 'chien8.jpg'),
(9, 'Bernard', 'Mâle', '2018-03-06', 'chien9.png'),
(10, 'Janine', 'Femelle', '2018-04-10', 'chien10.png'),
(19, 'Test sans image', 'Mâle', '2019-01-01', 'img_917246f_20190121_154033.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `chat`
--

DROP TABLE IF EXISTS `chat`;
CREATE TABLE IF NOT EXISTS `chat` (
  `id_animal` int(11) NOT NULL,
  `pelage` varchar(20) NOT NULL,
  PRIMARY KEY (`id_animal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `chat`
--

INSERT INTO `chat` (`id_animal`, `pelage`) VALUES
(1, 'Noir et Blanc'),
(2, 'Roux'),
(3, 'Roux et Blanc'),
(4, 'Gris'),
(5, 'Noir et Blanc'),
(19, '');

-- --------------------------------------------------------

--
-- Structure de la table `chien`
--

DROP TABLE IF EXISTS `chien`;
CREATE TABLE IF NOT EXISTS `chien` (
  `id_animal` int(11) NOT NULL,
  `race` varchar(20) NOT NULL,
  PRIMARY KEY (`id_animal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `chien`
--

INSERT INTO `chien` (`id_animal`, `race`) VALUES
(6, 'Labrador'),
(7, 'Berger Allemand'),
(8, 'Bichon'),
(9, 'Chihuahua'),
(10, 'Colley');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`id_animal`) REFERENCES `animal` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `chien`
--
ALTER TABLE `chien`
  ADD CONSTRAINT `chien_ibfk_1` FOREIGN KEY (`id_animal`) REFERENCES `animal` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
